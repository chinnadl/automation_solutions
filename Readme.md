**Aware Automation Framework/Solution Consist of 6 Modules:**
1. Keywords
2. Library
3. Resources
4. Testsuites
5. test_runner.py
6. Reports

**Keywords** *Scope: Automation Developers only* \
Contains Framework level Common keywords that can be used by that
specific modules.

**Library** *Scope: Automation Developers only* \
Contains the Python/Robot Library files that can be accessed by Keywords.

**Resources** *Scope: Testers/Automation Developers* \
Contains the data that are customised or mandatory to perform the test.

**Testsuites** *Scope: Testers/Automation Developers* \
Comprises of testsuites, testcases of various purposes.

**test_runner.py** *Scope: Automation Developers only* \
Acts as execution engine to kick start the test and handles the
additional responsibilities of creating a folder/files etc \
From the Home directory Aware_Automation run similar command as mentioned
below from a terminal: \
*python test_runner.py testfile.robot*

testfile.robot - depicts the testsuite file name that is located in the
folder Aware_Automations/Testsuites/

Note: Python 3.8 (64 bit) & Robot framework Environment required.

**Reports** *Scope: Testers/Automation Developers* \
Contains the test report of executed testsuites, along with the
captured artifacts, logs, html report.


**Setup Requirements:**
1. Install Python Version 3.8.0 64 bit
2. Install Pycharm community version
3. Install IntelliBot Plugin : Goto -> File -> Settings -> Plugins -> IntelliBot
4. Install Robot Framework Support Plugin : Goto -> File -> Settings -> Plugins -> Robot Framework Support
5. Ensure the correct version of selenium web drivers are available on the machine that runs the test.